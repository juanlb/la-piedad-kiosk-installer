# Instrucciones para instalar un puesto en La Piedad

Las instrucciones para instalar están en dos formatos:
- En el PDF: `Kiosk_para_La_Piedad.pdf` que se ve en el listado superior 👆️
- En Google Docks [Kiosk_para_La_Piedad](https://docs.google.com/document/d/1gFVOBSkSpMlx4aapQ6dsAjqdyZ4ibqbCyeJP_mGBhWU/edit?usp=sharing)


Siguiendo esos pasos, se obtiene una compudora corriendo en modo Kiosk para ser utilizada como `Puesto de Venta` o `Caja` en el sistema de **Confiteria La Piedad**

