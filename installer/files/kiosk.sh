#!/bin/bash

# Run this script in display 0 - the monitor
export DISPLAY=:0

# Hide the mouse from the display
unclutter &

# If Chromium crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/kiosk/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/kiosk/.config/chromium/Default/Preferences

# Run Chromium and open tabs
/usr/bin/chromium-browser  --kiosk --window-position=0,0 http://192.168.1.250 &


# Start the kiosk loop. This keystroke changes the Chromium tab
# To have just anti-idle, use this line instead:
# xdotool keydown ctrl; xdotool keyup ctrl;

# 32 o 64 bits?
ARQ=`uname -m`
if [ $ARQ == 'x86_64' ];
then
# Start docker services for printing, security group, etc - 64 bits
cd /home/kiosk/lapiedad-docker-services
./run-docker-compose.sh
else
cd /home/kiosk/fp_python
sudo ./driver_server.py
fi
