#!/bin/bash

# Stop, get latests and start docker services for printing, security group, etc
cd /home/kiosk/lapiedad-docker-services
docker-compose down
docker-compose rm -f

# get the latest version of docker-compose.yml
cd /home/kiosk/repo-kiosk/la-piedad-kiosk-installer
git pull
if [ -f "/home/kiosk/repo-kiosk/la-piedad-kiosk-installer/installer/files/docker-compose.yml" ]
then
rm /home/kiosk/lapiedad-docker-services/docker-compose.yml
cp /home/kiosk/repo-kiosk/la-piedad-kiosk-installer/installer/files/docker-compose.yml /home/kiosk/lapiedad-docker-services
fi

cd /home/kiosk/lapiedad-docker-services
docker-compose pull
docker-compose up -d