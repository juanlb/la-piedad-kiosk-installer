#!/bin/bash

set -e

sudo apt update
sudo apt upgrade -y

sudo apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    vim openssh-server chromium-browser unclutter xdotool

# 32 o 64 bits?
ARQ=`uname -m`

################################## User kiosk

if [ ! -d "/home/kiosk" ] 
then
username='kiosk'
password='kiosk.pass'
sudo adduser --disabled-password --gecos "" "$username"

echo $username:$password | sudo chpasswd
echo 'kiosk ALL=(ALL:ALL) NOPASSWD: ALL' | sudo EDITOR='tee -a' visudo
fi

################################## Root pass
sudo sh -c 'echo root:root.pass | chpasswd'

################################## Permitir entrar por ssh
if ! grep -q "PermitRootLogin yes" /etc/ssh/sshd_config; then
  echo 'PermitRootLogin yes' | sudo tee -a /etc/ssh/sshd_config
  sudo service sshd restart
fi

if [ $ARQ == 'x86_64' ];
then
  ################################## Docker
  if [ ! -f "/usr/share/keyrings/docker-archive-keyring.gpg" ]
  then 
  # docker repository key
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  fi

  # repositorio
  if [ ! -f "/etc/apt/sources.list.d/docker.list" ] 
  then
  echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  fi

  # Instalar
  sudo apt update
  sudo apt install -y docker-ce docker-ce-cli containerd.io

  # Quitar la necesidad de hacer "sudo"
  sudo usermod -aG docker $USER
  sudo usermod -aG docker kiosk

  ################################## Docker compose
  sudo cp ./files/docker-compose /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose

  sudo mkdir -p /home/kiosk/lapiedad-docker-services
  sudo cp ./files/docker-compose.yml /home/kiosk/lapiedad-docker-services
  sudo cp ./files/run-docker-compose.sh /home/kiosk/lapiedad-docker-services
  sudo chown -R kiosk:kiosk /home/kiosk/lapiedad-docker-services
else
  sudo apt install -y python-pip

  sudo mkdir -p /home/kiosk/fp_python
  sudo cp ./files/driver_server.tar /home/kiosk/fp_python
  sudo cp ./files/requirements.txt /home/kiosk/fp_python
  cd /home/kiosk/fp_python
  sudo tar -xf driver_server.tar
  cd -
  sudo rm /home/kiosk/fp_python/driver_server.tar
  sudo chown -R kiosk:kiosk /home/kiosk/fp_python

  sudo -H -u kiosk bash -c 'pip install -r /home/kiosk/fp_python/requirements.txt'
fi

################################## Copiar archivos

sudo cp ./files/lightdm.conf /etc/lightdm/

sudo mkdir -p /etc/lightdm/lightdm.conf.d
sudo cp ./files/50-myconfig.conf /etc/lightdm/lightdm.conf.d/

sudo mkdir -p /home/kiosk/.config/autostart
sudo cp ./files/kiosk.desktop /home/kiosk/.config/autostart/

sudo cp ./files/kiosk.sh /home/kiosk
sudo chown kiosk:kiosk /home/kiosk/kiosk.sh
sudo chmod +x /home/kiosk/kiosk.sh

################################## Repo
if [ $ARQ == 'x86_64' ];
then
sudo mkdir -p /home/kiosk/repo-kiosk
sudo chown -R $USER:$USER /home/kiosk/repo-kiosk
cd /home/kiosk/repo-kiosk
git clone https://gitlab.com/juanlb/la-piedad-kiosk-installer.git
sudo chown -R kiosk:kiosk /home/kiosk/repo-kiosk
fi

################################## Limpiar y Reiniciar el equipo
sudo apt remove -y gnome-keyring
sudo apt autoremove -y
sudo reboot